/*
** =============================================================================
** Copyright (c) 2016  Texas Instruments Inc.
**
** This program is free software; you can redistribute it and/or modify it under
** the terms of the GNU General Public License as published by the Free Software 
** Foundation; version 2.
**
** This program is distributed in the hope that it will be useful, but WITHOUT
** ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
** FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
**
** You should have received a copy of the GNU General Public License along with
** this program; if not, write to the Free Software Foundation, Inc., 51 Franklin
** Street, Fifth Floor, Boston, MA 02110-1301, USA.
**
** File:
**     tas2555_lib.h
**
** Description:
**     header file for tas2555_lib.c
**
** =============================================================================
*/

#ifndef TAS2555_LIB_H_
#define TAS2555_LIB_H_

#include <stdint.h>
#include <stdbool.h>

int get_lib_ver();

double get_f0(unsigned int fs, unsigned int nA1, unsigned int nA2);
double get_re(unsigned int nRe);

#endif /* TAS2555_LIB_H_ */
